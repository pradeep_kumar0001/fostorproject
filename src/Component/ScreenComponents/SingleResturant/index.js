import React from 'react';
//          Importing CSS Files
import "./style.css";
//          Importing Components
import { Row, Col, Container } from 'react-bootstrap';
//          Importing Controllers
import MyResturantController from '../../../Controller/ResturantController';
//          Importing Redux Functions
import { connect } from "react-redux";
import { Header } from '../../Header';
import {WhatsappShareButton,FacebookShareButton} from "react-share";
import {FacebookIcon,WhatsappIcon} from 'react-share'

class SingleResturantDetail extends React.Component {
    componentDidMount () {
        const { user, history, match } = { ...this.props };
        if ( user.token ) {
            this._handleLoadResturant( match.params.resturantId ? match.params.resturantId : "" );
        } else {
            history.push( "/" );
        }
    };
    _handleLoadResturant = ( resturantId ) => {
        MyResturantController.getSingleResturant( resturantId );
    };
    render() { 
        const { singleResturant , history } = { ...this.props };
        return (
            <Container>
            <Header _handleLogout={ () => { history.push( "/" ) } } />
            <div>
            <FacebookShareButton url={singleResturant.images ? singleResturant.images[ 0 ].url : ""}>
            <FacebookIcon size={32} round={true} />
            </FacebookShareButton>
            <WhatsappShareButton url={singleResturant.images ? singleResturant.images[ 0 ].url : ""}>
            <WhatsappIcon size={32} round={true} />
            </WhatsappShareButton>
            
            </div>
                <Row className="signle-rest">
                    <Col xl={12}>
                        <img src={ singleResturant.images ? singleResturant.images[ 0 ].url : "" } alt='rest-img' className="rest-image" />
                    </Col>
                </Row>
                <div className='rest'>
                    <div className='rest-details'>
                        <h1 className='rest-title'>{ singleResturant.restaurant_name ? singleResturant.restaurant_name : "" }</h1>
                    <div className="rest-reviewsSection">
                        <h1 className="rest-rating ">{ singleResturant.rating.restaurant_avg_rating ? singleResturant.rating.restaurant_avg_rating : 0 }</h1>
                        <h1 className="rest-reviewsSection-title">Delivery Reviews</h1>
                    </div>
                    </div>
                    <div className='rest-desc'>
                        <h2 className='rest-address'>{ singleResturant.currency.symbol ? singleResturant.currency.symbol : "" } { singleResturant.avg_cost_for_two ? singleResturant.avg_cost_for_two : "0" } FOR TWO</h2>
                        <h2 className='rest-address'>{ singleResturant.location.location_locality ? singleResturant.location.location_locality : "" } { singleResturant.location.city_name ? singleResturant.location.city_name : "" }</h2>
                        <h2 className='rest-openTime'>{ singleResturant.opens_at ? singleResturant.opens_at : "" } AM – { singleResturant.closes_at ? singleResturant.closes_at : "" } PM (Today)</h2>
                    </div>
                    <hr/>
                </div>
            </Container>
        );
    }
}
const mapStateToProps = ( state ) => ( {
    user: state.AuthReducer.user,
    singleResturant: state.ResturantReducer.singleResturant,
} );
export default connect( mapStateToProps, null )( SingleResturantDetail );