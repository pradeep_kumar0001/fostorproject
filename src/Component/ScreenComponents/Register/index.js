import React from 'react';
//          Importing Components
import { Link } from "react-router-dom";
//          Importing Controllers
import MyAuthController from "../../../Controller/AuthController";
//          Importing Redux Functions
import { connect } from "react-redux";

class RegisterScreen extends React.Component {
    state = {
        dialCode: "",
        number: "",
        errorMessage: "",
        loading: false,
    };
    _handleRegister = ( e ) => {
        e.preventDefault();
        const { dialCode, number } = { ...this.state };
        const { history } = { ...this.props };
        if ( dialCode === "" ) {
            this.setState( { errorMessage: "* please enter dial code" } );
        } else if ( number === "" ) {
            this.setState( { errorMessage: "* please enter number" } );
        } else {
            this.setState( { loading: true } );
            MyAuthController.sendOtp( number, dialCode ).then( res => {
                this.setState( { loading: false, errorMessage: "" } );
                history.push( "/" );
            } ).catch( err => {
                this.setState( { loading: false, errorMessage: err } );
            } );
        }
    };
    componentDidMount () {
        const { user, history } = { ...this.props };
        if ( user.token ) { history.push( "/Resturants" ) }
    };
    render () {
        const { dialCode, number, errorMessage, loading } = { ...this.state };
        return (
            <div className="card" style={ { ...Style.cardContainer } }>
                <div className="card-body">
                    <h5 className="card-title text-center">Register</h5>
                    <form onSubmit={ ( e ) => { this._handleRegister( e ) } }>
                        <div className="mt-3">
                            <label for="dial_code" className="form-label">Dial Code</label>
                            <input type="text" className="form-control" id="dial_code" placeholder="+91" disabled={ loading } value={ dialCode } onChange={ ( e ) => { this.setState( { dialCode: e.target.value, errorMessage: "" } ) } } />
                        </div>
                        <div className="mt-3">
                            <label for="phone_number" className="form-label">Number</label>
                            <input type="number" className="form-control" id="phone_number" placeholder="92834871239" disabled={ loading } value={ number } onChange={ ( e ) => { this.setState( { number: e.target.value, errorMessage: "" } ) } } />
                        </div>
                        { errorMessage ? <div className="mt-3" style={ { color: "red", fontSize: "12px", textAlign: "center" } }>{ errorMessage }</div> : "" }
                        <button className="btn btn-primary w-100 mt-3" disabled={ loading } type="submit">
                            Register
                        </button>
                    </form>
                    <div className="mt-3 text-center">Already have account? <Link to="/">Login</Link></div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = ( state ) => ( { user: state.AuthReducer.user } );
export default connect( mapStateToProps, null )( RegisterScreen );
const Style = {
    cardContainer: { width: "25rem", position: "absolute", left: "50%", top: "50%", transform: "translate(-50%, -50%)" },
};