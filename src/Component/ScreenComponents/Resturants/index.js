import React from 'react';
//          Importing Components
import { Row, Col } from 'react-bootstrap';
import { Header } from '../../Header';
import TopBrands from '../../TopBrands';
import { ResturentCard } from "../../ResturentCard";
//          Importing Controllers
import MyResturantController from "../../../Controller/ResturantController";
//          Importing Redux Functions
import { connect } from "react-redux";

class ResturantScreen extends React.Component {
    _handleLoadResturants = () => {
        MyResturantController.getResturants().then( res => { } ).catch( err => { } );
    };
    componentDidMount () {
        const { user, history } = { ...this.props };
        if ( user.token ) {
            this._handleLoadResturants();
        } else {
            history.push( "/" );
        }
    };
    render() {
        const { resturantsList, history } = { ...this.props };
        return (
            <>
                <Header _handleLogout={ () => { history.push( "/" ) } } />
                <TopBrands/>
                <Row>
                    { resturantsList.length > 0 ?
                        resturantsList.map( resturant =>
                            <Col xl={ 4 }>
                                <ResturentCard
                                    resturant={ resturant }
                                    onClick={ () => { history.push( `/ResturantDetail/${ resturant.restaurant_id ? resturant.restaurant_id : "" }` ) } }
                                />
                            </Col> ) : "" }
                </Row>
            </>
        );
    }
}
const mapStateToProps = ( state ) => ( {
    user: state.AuthReducer.user,
    resturantsList: state.ResturantReducer.resturantsList
} );
export default connect( mapStateToProps, null )( ResturantScreen );