import React from 'react';
import { Container } from 'react-bootstrap';
import './ResturentCard.css'



export const ResturentCard = (props) => {
  const { resturant, onClick } = { ...props };
  
  return (
   <Container style={{marginTop:"2.5rem",marginBottom:"2.5rem"}} onClick={ onClick ? onClick : () => { } }>
               <div className="explore-card cur-po explore-card-first" >
    <div className="explore-card-cover">
      <img
        src={ resturant.images ? resturant.images[ 0 ].url : "" }
        className="explore-card-image"
        alt="restaurant"
      />
      <div className="delivery-time">30 Min</div>
      <div className="pro-off">Pro 65%</div>
      <div className="discount absolute-center">discount 10%</div>
    </div>
    <div className="res-row">
      <div className="res-name"> { resturant.restaurant_name ? resturant.restaurant_name : "" }</div>
      
        <div className="res-rating absolute-center">
        { resturant.rating.restaurant_avg_rating ? resturant.rating.restaurant_avg_rating : 0 }<i className="fi fi-sr-star absolute-center" />
        </div>
    </div>
    <div className="res-row">
        <div className="res-cuisine">
            <span className="res-cuisine-tag">
            North Indian, Chinese
            </span>
        </div>
      <div className="res-price">Avg for Two { resturant.currency.symbol ? resturant.currency.symbol : "" } { resturant.avg_cost_for_two ? resturant.avg_cost_for_two : "0" }</div>
    </div>
      <div>
        <div className="card-separator"></div>
        <div className="explore-bottom">
          <img
            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png?output-format=webp"
            alt="img"
            style={{ height: "18px" }}
          />
          <div className="res-bottom-text">1453+ orders placed from here recently</div>
        </div>
      </div>
  </div>
   </Container>
  );
}
