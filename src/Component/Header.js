import React from 'react'
import { Container } from 'react-bootstrap'
import './Header.css'
//         Importing Controllers
import MyAuthController from "../Controller/AuthController";

export const Header = ( props ) => {
  const { _handleLogout } = { ...props };
  return (
    <Container>
      <div className="header max-width">
      <img
        src="https://logos-world.net/wp-content/uploads/2020/11/Zomato-Logo.png"
        alt="Zomato-logo"
        className="header-logo"
      />
      <div className="header-right">
        <div className="header-location-search-container">
          <div className="location-wrapper">
            <div className="location-icon-name">
              <i className="fi fi-rr-marker absolute-center location-icon"></i>
              <div>Delhi</div>
            </div>
            <i className="fi fi-rr-caret-down absolute-center"></i>
          </div>
          <div className="location-search-separator"></div>
          <div className="header-searchBar">
            <i className="fi fi-rr-search absolute-center search-icon"></i>
            <input
              className="search-input"
              placeholder="Search for restaurant, cuisine or a dish"
            />
          </div>
        </div>

        <div className="profile-wrapper">
            <button className="btn btn-primary logout" onClick={ () => { MyAuthController.logout().then( res => { if ( _handleLogout ) { _handleLogout() } } ) } }>
              Logout
            </button>
        </div>
      </div>
    </div>
    </Container>    
  )
};
