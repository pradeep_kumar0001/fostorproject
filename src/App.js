import React from 'react'
//          Importing Routing
import Routing from "./Routing";
//          Importing Redux Functions
import { Provider } from "react-redux";
import store from "./Store/store";

function App() {
  return (
    <Provider store={ store }>
      <Routing />
    </Provider>
  )
}

export default App
