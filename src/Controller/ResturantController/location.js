class Location {
    constructor() {
        this.toJSON = () => {
            return {
                location_id: 0,
                location_address: "",
                location_address_2: "",
                location_zip_code: 0,
                location_lat: 0,
                location_long: 0,
                location_locality: "",
                city_name: "",
                state_name: "",
                city_id: "",
                country_id: "",
                state_id: "",
                update_count: "",
            };
        };
        this.fromSourceToJSON = ( location ) => {
            return {
                location_id: location.location_id ? location.location_id : 0,
                location_address: location.location_address ? location.location_address : "",
                location_address_2: location.location_address_2 ? location.location_address_2 : "",
                location_zip_code: location.location_zip_code ? location.location_zip_code : 0,
                location_lat: location.location_lat ? location.location_lat : 0,
                location_long: location.location_long ? location.location_long : 0,
                location_locality: location.location_locality ? location.location_locality : "",
                city_name: location.city_name ? location.city_name : "",
                state_name: location.state_name ? location.state_name : "",
                city_id: location.city_id ? location.city_id : "",
                country_id: location.country_id ? location.country_id : "",
                state_id: location.state_id ? location.state_id : "",
                update_count: location.update_count ? location.update_count : "",
            };
        };
    }
}
const MyLocation = new Location();
export default MyLocation;