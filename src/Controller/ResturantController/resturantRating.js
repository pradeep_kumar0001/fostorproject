class Rating {
    constructor() {
        this.toJSON = () => {
            return {
                restaurant_avg_rating: "",
                count: 0,
            };
        };
        this.fromSourceToJSON = ( rating ) => {
            return {
                restaurant_avg_rating: rating.restaurant_avg_rating ? rating.restaurant_avg_rating : "",
                count: rating.count > 0 ? rating.count : 0,
            };
        };
    }
}
const MyRating = new Rating();
export default MyRating;