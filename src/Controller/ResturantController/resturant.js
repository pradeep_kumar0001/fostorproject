import MyRating from "./resturantRating";
import MyCurrency from "./currency";
import MyLocation from "./location";

class Resturant {
    constructor() {
        this.toJSON = () => {
            return {
                restaurant_id: "",
                lead_id: "",
                restaurant_name: "",
                avg_cost_for_two: "",
                avg_order_cost: "",
                avg_daily_order_count: "",
                rating: MyRating.toJSON(),
                currency: MyCurrency.toJSON(),
                table_count: "",
                does_bookings: false,
                location_id: "",
                e_type_id: "",
                restaurant_uuid: "",
                image: "",
                restaurant_mode: "",
                opens_at: "",
                closes_at: "",
                qr_active: false,
                address_complete: "",
                is_close: false,
                is_close_cafeteria: "",
                status: "",
                merchant_payment_methods: [],
                social_profiles: "",
                logo: "",
                next_closes_at: "",
                next_opens_at: "",
                free_trial_eligiblity: false,
                points: "",
                active: false,
                free_tier_expire_at: "",
                refer_responded: false,
                active_plan: "",
                free_trial_expired: false,
                created_at: "",
                restaurant_code: "",
                noti_set: "",
                activated_at: "",
                active_plan_id: "",
                type_id: "",
                m_id: "",
                location: MyLocation.toJSON(),
                restaurant_type: "",
                cuisines: [],
                images: "",
                thumbnail_image: "",
                cover_image: "",
                small_image: "",
                large_image: "",
            };
        };
        this.fromSourceToJSON = ( resturant ) => {
            return {
                restaurant_id: resturant.restaurant_id ? resturant.restaurant_id : "",
                lead_id: resturant.lead_id ? resturant.lead_id : "",
                restaurant_name: resturant.restaurant_name ? resturant.restaurant_name : "",
                avg_cost_for_two: resturant.avg_cost_for_two ? resturant.avg_cost_for_two : "",
                avg_order_cost: resturant.avg_order_cost ? resturant.avg_order_cost : "",
                avg_daily_order_count: resturant.avg_daily_order_count ? resturant.avg_daily_order_count : "",
                rating: resturant.rating ? MyRating.fromSourceToJSON( resturant.rating ) : MyRating.toJSON(),
                currency: resturant.currency ? MyCurrency.fromSourceToJSON( resturant.currency ) : MyCurrency.toJSON(),
                table_count: resturant.table_count ? resturant.table_count : "",
                does_bookings: resturant.does_bookings ? true : false,
                location_id: resturant.location_id ? resturant.location_id : "",
                e_type_id: resturant.e_type_id ? resturant.e_type_id : "",
                restaurant_uuid: resturant.restaurant_uuid ? resturant.restaurant_uuid : "",
                image: resturant.image ? resturant.image : "",
                restaurant_mode: resturant.restaurant_mode ? resturant.restaurant_mode : "",
                opens_at: resturant.opens_at ? resturant.opens_at : "",
                closes_at: resturant.closes_at ? resturant.closes_at : "",
                qr_active: resturant.qr_active ? true : false,
                address_complete: resturant.address_complete ? resturant.address_complete : "",
                is_close: resturant.is_close ? true : false,
                is_close_cafeteria: resturant.is_close_cafeteria ? resturant.is_close_cafeteria : "",
                status: resturant.status ? resturant.status : "",
                merchant_payment_methods: resturant.merchant_payment_methods ? resturant.merchant_payment_methods : [],
                social_profiles: resturant.social_profiles ? resturant.social_profiles : "",
                logo: resturant.logo ? resturant.logo : "",
                next_closes_at: resturant.next_closes_at ? resturant.next_closes_at : "",
                next_opens_at: resturant.next_opens_at ? resturant.next_opens_at : "",
                free_trial_eligiblity: resturant.free_trial_eligiblity ? true : false,
                points: resturant.points ? resturant.points : "",
                active: resturant.active ? true : false,
                free_tier_expire_at: resturant.free_tier_expire_at ? resturant.free_tier_expire_at : "",
                refer_responded: resturant.refer_responded ? true : false,
                active_plan: resturant.active_plan ? resturant.active_plan : "",
                free_trial_expired: resturant.free_trial_expired ? true : false,
                created_at: resturant.created_at ? resturant.created_at : "",
                restaurant_code: resturant.restaurant_code ? resturant.restaurant_code : "",
                noti_set: resturant.noti_set ? resturant.noti_set : "",
                activated_at: resturant.activated_at ? resturant.activated_at : "",
                active_plan_id: resturant.active_plan_id ? resturant.active_plan_id : "",
                type_id: resturant.type_id ? resturant.type_id : "",
                m_id: resturant.m_id ? resturant.m_id : "",
                location: resturant.location ? MyLocation.fromSourceToJSON( resturant.location ) : MyLocation.toJSON(),
                restaurant_type: resturant.restaurant_type ? resturant.restaurant_type : "",
                cuisines: resturant.cuisines ? resturant.cuisines : [],
                images: resturant.images ? resturant.images : "",
                thumbnail_image: resturant.thumbnail_image ? resturant.thumbnail_image : "",
                cover_image: resturant.cover_image ? resturant.cover_image : "",
                small_image: resturant.small_image ? resturant.small_image : "",
                large_image: resturant.large_image ? resturant.large_image : "",
            };
        };
    }
}
const MyResturant = new Resturant();
export default MyResturant;