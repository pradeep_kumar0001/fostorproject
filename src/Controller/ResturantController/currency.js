class Currency {
    constructor() {
        this.toJSON = () => {
            return {
                symbol: "",
            };
        };
        this.fromSourceToJSON = ( currency ) => {
            return {
                symbol: currency.symbol ? currency.symbol : "",
            };
        };
    }
}
const MyCurrency = new Currency();
export default MyCurrency;