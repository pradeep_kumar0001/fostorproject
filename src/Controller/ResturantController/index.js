import { GET_RESTURANTS, GET_SINGLE_RESTURANTS } from "../actionTypes";
import { BaseURL } from "../baseURL";
import axios from "axios";
import store from "../../Store/store";
import MyResturant from "./resturant";

class ResturantController {
    constructor() {
        this.getResturants = () => {
            return new Promise( ( resolve, reject ) => {
                console.log( "TEST-GET-RESTURANTS: API CALLING" );
                const accessToken = store.getState().AuthReducer.user.token;
                axios( {
                    method: "GET",
                    url: `${ BaseURL }/v1/m/restaurant`,
                    headers: {
                        Authorization: `Bearer ${ accessToken }`,
                    },
                } ).then( res => {
                    let resturantsList = [];
                    for ( let index = 0; index < res.data.length; index++ ) {
                        const resturant = res.data[ index ];
                        resturantsList.push( MyResturant.fromSourceToJSON( resturant ) );
                    }
                    store.dispatch( { type: GET_RESTURANTS, payload: { resturantsList: resturantsList } } );
                    resolve( { resturantsList: resturantsList } );
                    console.log( "TEST-GET-RESTURANTS: API SUCCESS" );
                } ).catch( err => {
                    store.dispatch( { type: GET_RESTURANTS, payload: { resturantsList: [] } } );
                    console.log( "TEST-GET-RESTURANTS: API FAIL" );
                    console.log( "NETWORK ERROR, ResturantsList, getResturants" );
                    console.log( err );
                    reject( "NETWORK ERROR" );
                } );
            } );
        };
        this.getSingleResturant = ( resturantId ) => {
            return new Promise( ( resolve, reject ) => {
                let rResturantsList = [];
                rResturantsList = store.getState().ResturantReducer.resturantsList;
                if ( rResturantsList.length > 0 ) {
                    for ( let index = 0; index < rResturantsList.length; index++ ) {
                        let resturant = rResturantsList[ index ];
                        if ( resturant.restaurant_id === resturantId ) {
                            store.dispatch( { type: GET_SINGLE_RESTURANTS, payload: { singleResturant: resturant } } )
                            resolve( { singleResturant: resturant } );
                            break;
                        }
                    }
                } else {
                    this.getResturants().then( res => {
                        for ( let index = 0; index < res.resturantsList.length; index++ ) {
                            let resturant = res.resturantsList[ index ];
                            if ( resturant.restaurant_id === resturantId ) {
                                store.dispatch( { type: GET_SINGLE_RESTURANTS, payload: { singleResturant: resturant } } )
                                resolve( { singleResturant: resturant } );
                                break;
                            }
                        }
                    } ).catch( err => {
                        reject( err );
                    } );
                }
            } );
        };
    };
}
const MyResturantController = new ResturantController();
export default MyResturantController;
