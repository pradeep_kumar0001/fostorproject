class User {
    constructor() {
        this.toJSON = () => {
            return {
                user_id: "",
                user_name: "",
                user_gender: "",
                user_email: "",
                dial_code: "",
                user_mobile: "",
                member_since: "",
                email_verified: "",
                mobile_verified: "",
                user_username: "",
                password: "",
                last_password_update: "",
                user_source: "",
                user_image: "",
                dob: "",
                first_order: "",
                refer_code: "",
                favorite: "",
                food_tags: "",
                workmode: "",
                country_id: "",
                last_known_location: "",
                token: "",
                refresh_token: "",
            };
        };
        this.fromSourceToJSON = ( user ) => {
            return {
                user_id: user.user_id ? user.user_id : "",
                user_name: user.user_name ? user.user_name : "",
                user_gender: user.user_gender ? user.user_gender : "",
                user_email: user.user_email ? user.user_email : "",
                dial_code: user.dial_code ? user.dial_code : "",
                user_mobile: user.user_mobile ? user.user_mobile : "",
                member_since: user.member_since ? user.member_since : "",
                email_verified: user.email_verified ? user.email_verified : "",
                mobile_verified: user.mobile_verified ? user.mobile_verified : "",
                user_username: user.user_username ? user.user_username : "",
                password: user.password ? user.password : "",
                last_password_update: user.last_password_update ? user.last_password_update : "",
                user_source: user.user_source ? user.user_source : "",
                user_image: user.user_image ? user.user_image : "",
                dob: user.dob ? user.dob : "",
                first_order: user.first_order ? user.first_order : "",
                refer_code: user.refer_code ? user.refer_code : "",
                favorite: user.favorite ? user.favorite : "",
                food_tags: user.food_tags ? user.food_tags : "",
                workmode: user.workmode ? user.workmode : "",
                country_id: user.country_id ? user.country_id : "",
                last_known_location: user.last_known_location ? user.last_known_location : "",
                token: user.token ? user.token : "",
                refresh_token: user.refresh_token ? user.refresh_token : "",
            };
        };
    }
}
const MyUser = new User();
export default MyUser;