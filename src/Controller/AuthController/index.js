import { LOGIN, LOGOUT } from "../actionTypes";
import { BaseURL } from "../baseURL";
import axios from "axios";
import store from "../../Store/store";
import MyUser from "./user";

class AuthController {
    constructor() {
        this.sendOtp = ( phone, dial_code ) => {
            return new Promise( ( resolve, reject ) => {
                console.log( "TEST-SENT-OTP: API CALLING" );
                const url = `${ BaseURL }/v1/pwa/user/register`;
                const config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
                const params = new URLSearchParams();
                params.append( 'phone', phone );
                params.append( 'dial_code', dial_code );
                axios.post( url, params, config ).then( res => {
                    if ( res.data.status === "Success" ) {
                        resolve( { codeSent: true } );
                        console.log( "TEST-SENT-OTP: API SUCCESS" );
                    } else {
                        reject( res.data.error_message );
                        console.log( "TEST-SENT-OTP: API FAIL" );
                    }
                } ).catch( err => {
                    console.log( "TEST-SENT-OTP: API FAIL" );
                    console.log( "NETWORK ERROR, AuthController, sendOtp" );
                    console.log( err );
                    reject( "NETWORK ERROR" );
                } );
            } )
        };
        this.login = ( phone, otp, dial_code ) => {
            return new Promise( ( resolve, reject ) => {
                console.log( "TEST-LOGIN: API CALLIGN" );
                const url = `${ BaseURL }/v1/pwa/user/login`;
                const config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
                const params = new URLSearchParams();
                params.append( 'phone', phone );
                params.append( 'dial_code', dial_code );
                params.append( 'otp', otp );
                axios.post( url, params, config ).then( res => {
                    if ( res.data.status === "Success" ) {
                        store.dispatch( { type: LOGIN, payload: { user: MyUser.fromSourceToJSON( res.data.data ) } } );
                        resolve( { user: MyUser.fromSourceToJSON( res.data.data ) } );
                        console.log( "TEST-LOGIN: API SUCCESS" );
                    } else {
                        reject( res.data.error_message );
                        console.log( "TEST-LOGIN: API FAIL" );
                    }
                } ).catch( err => {
                    console.log( "TEST-LOGIN: API FAIL" );
                    console.log( "NETWORK ERROR, AuthController, login" );
                    console.log( err );
                    reject( "NETWORK ERROR" );
                } );
            } );
        };
        this.logout = () => {
            return new Promise( ( resolve, reject ) => {
                store.dispatch( { type: LOGOUT } );
                resolve( "LOGOUT SUCCESSFULLY" );
            } );
        };
    }
}
const MyAuthController = new AuthController();
export default MyAuthController;