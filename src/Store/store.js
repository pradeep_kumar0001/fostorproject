import { createStore, compose, applyMiddleware } from "redux";
import RootReducer from "./index";
import thunk from "redux-thunk";
const initialState = {};
const middleWares = [thunk];
let store = null;
if ( window.__REDUX_DEVTOOLS_EXTENSION__ ) {
    store = createStore( RootReducer, initialState, compose( applyMiddleware( ...middleWares ), window.__REDUX_DEVTOOLS_EXTENSION__() ) );
} else {
    store = createStore( RootReducer, initialState, compose( applyMiddleware( ...middleWares ) ) );
}
export default store;
