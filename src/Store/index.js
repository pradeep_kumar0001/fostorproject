import { combineReducers } from "redux";
//          Importing Reducers
import AuthReducer from "../Reducers/authReducer";
import ResturantReducer from "../Reducers/resturantReducer";

const rootReducer = combineReducers( {
    AuthReducer,
    ResturantReducer
} );

export default rootReducer;