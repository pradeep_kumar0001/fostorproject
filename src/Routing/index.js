import React from 'react';
import { BrowserRouter as Router, Switch, Route, withRouter } from "react-router-dom";
import ScrollToTop from "./scrollToTop";
//           Importing Client Screens
import Login from "../Component/ScreenComponents/Login";
import Register from "../Component/ScreenComponents/Register";
import Resturants from "../Component/ScreenComponents/Resturants";
import ResturantDetail from "../Component/ScreenComponents/SingleResturant";

class Routing extends React.PureComponent {
    state = {};
    render() { 
        return (
            <Router>
                <ScrollToTop>
                    <Switch>
                        <Route exact path="/" component={ withRouter( Login ) } />
                        <Route path="/Register" component={ withRouter( Register ) } />
                        <Route path="/Resturants" component={ withRouter( Resturants ) } />
                        <Route path="/ResturantDetail/:resturantId" component={ withRouter( ResturantDetail ) } />
                    </Switch>
                </ScrollToTop>
            </Router>
        );
    }
}
export default Routing;