import { GET_RESTURANTS, GET_SINGLE_RESTURANTS, LOGOUT } from "../Controller/actionTypes";
import { produce } from "immer";
import MyResturant from "../Controller/ResturantController/resturant";

const state = {
    resturantsList: [],
    singleResturant: MyResturant.toJSON(),
};

function ResturantReducer ( mState = { ...state }, action ) {
    switch ( action.type ) {
        case GET_RESTURANTS:
            return produce( mState, draftState => { draftState.resturantsList = action.payload.resturantsList } );
        
        case GET_SINGLE_RESTURANTS:
            return produce( mState, draftState => { draftState.singleResturant = action.payload.singleResturant } );
        
        case LOGOUT:
            return produce( mState, draftState => {
                draftState.resturantsList = [];
                draftState.singleResturant = MyResturant.toJSON();
            } );
        
        default:
            return { ...mState };
    }
}
export default ResturantReducer;