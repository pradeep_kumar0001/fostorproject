import { REGISTER_AND_GET_OTP, LOGIN, LOGOUT } from "../Controller/actionTypes";
import { produce } from "immer";
import MyUser from "../Controller/AuthController/user";

const state = {
    user: MyUser.toJSON(),
};

function AuthReducer ( mState = { ...state }, action ) {
    switch ( action.type ) {
        case REGISTER_AND_GET_OTP:
            return produce( mState, draftState => {
                AuthReducerPersistance( "--user--", draftState );    
            } );
        
        case LOGIN:
            return produce( mState, draftState => {
                draftState.user = action.payload.user;
                AuthReducerPersistance( "--user--", draftState );
            } );
        
        case LOGOUT:
            return produce( mState, draftState => {
                draftState.user = MyUser.toJSON();
                AuthReducerPersistance( "--user--", draftState );
            } );
        
        default:
            if ( localStorage.getItem( "--user--" ) ) { return JSON.parse( localStorage.getItem( "--user--" ) ) }
            else { return { ...mState } }
    }
}
const AuthReducerPersistance = ( key, data ) => {
    localStorage.setItem( key, JSON.stringify( data ) );
};
export default AuthReducer;